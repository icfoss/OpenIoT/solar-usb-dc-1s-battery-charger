# Solar USB DC 1S Battery Charger Board

Solar USB DC 1S Battery Charger repository contains hardware sources and documentation. The board takes in Solar, DC and USB inputs to charge 1S Battery with temperature protection. The board can drive an average load of upto 1A and peaks of upto 1.8 A. The boards is based on MCP73871 from Microchip. For more details see component [Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/20002090C.pdf) from manufacturer.

## Hardware Specifications

* Solar Panel input via Barrel Jack. Panel rating must not exceed 6V.
* Average load should be less than 1A.
* Battery Charging at 500mA. Replace PROG1 with 1K Resistor to increase charging current to 1A 
* Jumpers provided each for Power LED and Status (Charging,Done) to minimise power consumption when not required.
* Analog out does Battery Voltage * 0.5 to bring voltage below 3.3v for ADC input of uC. Additional Cap (0.1uF) provided for sample-and-hold circuit of ADC.
   
### Recommendations

* Solar Panel : 3.2Watts 6v 540mA Solar Panel.
* USB Wall Adapter : 5V 1A.
* Battery : 18650 (Li-ion 3.7/4.2V) or similar chemistry.
* Connector : [Barrel Jack](https://www.sparkfun.com/products/119) (Solar Input), USB (5V  DC Input)
* Connector : Load/ BAT : [JST PH 2P 2.0MM Socket](https://www.digikey.com/product-detail/en/jst-sales-america-inc/B2B-PH-SM4-TB-LF-SN/455-1734-1-ND/926831) needs [JST PH 2.0 Cable](https://www.banggood.in/100Pcs-Mini-Micro-JST-2_0-PH-2Pin-Connector-Plug-With-120mm-Wires-Cables-p-1147298.html). 
* **Note that Polarity of JST cable is opposite to that of [adafruit](https://www.adafruit.com/product/261)/ [sparkfun](https://www.sparkfun.com/products/9914) cables.**


## Contributing

Instructions coming up soon.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [USB / DC / Solar Lithium Ion/Polymer charger - v2](https://www.adafruit.com/product/390)

